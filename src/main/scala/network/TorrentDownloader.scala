package network

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import data.PvrShow
import scala.util.parsing.json.JSON
import torrents.TorrentClient
import scala.io.Source

object TorrentDownloader {

  def download(show: PvrShow, client: TorrentClient) = {

    println("Downloading torrent for " + show.name)
    val magnetLink = getMagnetLink(show)

    magnetLink match {
      case Some(link: String) => downloadShow(link)
      case None => println("TorrentDownloader: Could not get magnet link")
      case other => println("TorrentDownloader: Unknown result type: " + other)
    }

  }
  /**
   * Find the best torrent entry from a list.
   * Criteria are whether the torrent was uploaded by someone the user trusts and whether the uploader is trusted by the website.
   * Note: The torrent entries coming from apify are expected to be ordered by seeders
   */
  def findBestTorrent(show: PvrShow, torrentList: List[Map[String, Any]]): Map[String, Any] = {

    // Only trusted torrents go into this list 
    val trustedTorrents = for (
      entry <- torrentList;
      uploader: String = "" + entry("uploader");
      if show.trustedUploaders.exists(publisher => uploader equalsIgnoreCase uploader);
      if entry("trusted").asInstanceOf[Boolean]

    ) yield { entry }

    if (trustedTorrents.isEmpty) torrentList.head else trustedTorrents.head

  }

  /**
   * Get the best magnet link of a show.
   */
  private def getMagnetLink(show: PvrShow): Option[String] = {
    val tpbUrl = NetworkUtils.pirateBayUrl
    val queryUrl = tpbUrl + show.name.replaceAll(" ", "%20")

    // Expecting a JSON string as the site's response
    val siteResponse = Source.fromURL(new URL(queryUrl)).mkString
    val result = JSON.parseFull(siteResponse)
    result match {
      // Matches if the object wrapped in Some is a List of String to String mappings
      case Some(torrentList: List[Map[String, String]]) =>

        val bestTorrentEntry = findBestTorrent(show, torrentList)
        // Get the magnet link from the best entry and wrap return it as an Option
        Some(bestTorrentEntry("magnet").toString)

      // Return None if there was no magnet link found
      case None =>
        println("Parsing failed"); None
      case other => println("Unknown result: " + other); None

    }

    //   val jsonStr = """[{"id":8491939,"name":"Game of Thrones S03E08 HDTV x264-EVOLVE[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:cbc721c4d97950d5d7fec51ed6235f38b8e7ca43&dn=Game+of+Thrones+S03E08+HDTV+x264-EVOLVE%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-20 04:04","size":"358.07 MiB","seeders":21304,"leechers":835,"trusted":true},{"id":8468424,"name":"Game of Thrones S03E07 HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:a7cc7a2a5a179e7431ec7cddb72c2bc45d764cdd&dn=Game+of+Thrones+S03E07+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-13 04:27","size":"443.29 MiB","seeders":15307,"leechers":635,"trusted":true},{"id":8442869,"name":"Game of Thrones S03E06 HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:80fe5f4edee78f38aede5f221382f09dabbf2d8f&dn=Game+of+Thrones+S03E06+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-06 04:10","size":"369.73 MiB","seeders":12682,"leechers":541,"trusted":true},{"id":8421608,"name":"Game of Thrones S03E05 HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:b31b078f99903887a2708586c06f0203070f79fc&dn=Game+of+Thrones+S03E05+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-29 04:22","size":"406.45 MiB","seeders":7823,"leechers":403,"trusted":true},{"id":8325578,"name":"Game of Thrones S03E01 HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:c90db98c5aabe64dcd7f730d816e755242fffbd4&dn=Game+of+Thrones+S03E01+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-01 04:10","size":"385.94 MiB","seeders":7652,"leechers":346,"trusted":true},{"id":8374158,"name":"Game of Thrones S03E03 HDTV x264-EVOLVE [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:2eab1b79cf5a2aaccc913a5183b3ea7a2494e5eb&dn=Game+of+Thrones+S03E03+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-15 04:11","size":"396.29 MiB","seeders":7032,"leechers":330,"trusted":true},{"id":8397298,"name":"Game of Thrones S03E04 HDTV x264-EVOLVE[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:b062565c3fe518b94312b12c2851f8ce76055dca&dn=Game+of+Thrones+S03E04+HDTV+x264-EVOLVE%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-22 04:01","size":"391.37 MiB","seeders":6824,"leechers":286,"trusted":true},{"id":8352880,"name":"Game of Thrones S03E02 HDTV x264-2HD[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:9e403fc0d2a025f48873719d931f50d56c379a36&dn=Game+of+Thrones+S03E02+HDTV+x264-2HD%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-08 04:27","size":"478.5 MiB","seeders":6670,"leechers":326,"trusted":true},{"id":8468475,"name":"Game of Thrones S03E07 720p HDTV x264-EVOLVE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:d67b7f5a842d670159adf81680170c78d1e99792&dn=Game+of+Thrones+S03E07+720p+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-13 04:52","size":"1.46 GiB","seeders":6505,"leechers":494,"trusted":true},{"id":8491940,"name":"Game.of.Thrones.S03E08.720p.HDTV.x264-EVOLVE [PublicHD]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:1d7596cbca2cc5e888b05a92cad735768131a4ea&dn=Game.of.Thrones.S03E08.720p.HDTV.x264-EVOLVE+%5BPublicHD%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-20 04:05","size":"1.27 GiB","seeders":5566,"leechers":282,"trusted":true},{"id":8442921,"name":"Game of Thrones S03E06 720p HDTV x264-IMMERSE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:6034deca0b1ec18f11071bcdada0c70798d12680&dn=Game+of+Thrones+S03E06+720p+HDTV+x264-IMMERSE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-06 04:44","size":"1.06 GiB","seeders":4551,"leechers":263,"trusted":true},{"id":8397348,"name":"Game of Thrones S03E04 HDTV x264-EVOLVE [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:25ece4b15669f3cfa46f69fdf19eee0c788f802d&dn=Game+of+Thrones+S03E04+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-22 04:17","size":"391.37 MiB","seeders":3593,"leechers":170,"trusted":true},{"id":8397596,"name":"Game of Thrones S03E04 720p HDTV x264-EVOLVE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:b8b0e8c34e4af6dbe5ae2743191b48ae2b7f3867&dn=Game+of+Thrones+S03E04+720p+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-22 05:21","size":"1.33 GiB","seeders":3248,"leechers":253,"trusted":true},{"id":7326797,"name":"Game of Thrones - Season 2 Complete HDTV (x264)","category":"TV shows","magnet":"magnet:?xt=urn:btih:15f110e2945c35a04676d2bf078658852b29c31b&dn=Game+of+Thrones+-+Season+2+Complete+HDTV+%28x264%29&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"06-04 2012","size":"3.85 GiB","seeders":3169,"leechers":1125,"trusted":true},{"id":8374291,"name":"Game of Thrones S03E03 720p HDTV x264-EVOLVE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:9df8d2c876a69f523311c19ae14d6dcc6f6854d5&dn=Game+of+Thrones+S03E03+720p+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-15 05:13","size":"1.35 GiB","seeders":3117,"leechers":237,"trusted":true},{"id":8352926,"name":"Game of Thrones S03E02 720p HDTV x264-IMMERSE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:10f5e35a571e6606258de9be5d235c03c32c55b0&dn=Game+of+Thrones+S03E02+720p+HDTV+x264-IMMERSE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-08 05:06","size":"1.66 GiB","seeders":3079,"leechers":279,"trusted":true},{"id":8352901,"name":"Game of Thrones S03E02 HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:491a7dc031ba0db04b33e6d8d75c419236527167&dn=Game+of+Thrones+S03E02+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-08 04:50","size":"478.5 MiB","seeders":2437,"leechers":111,"trusted":true},{"id":8421878,"name":"Game of Thrones S03E05 PROPER 720p HDTV x264-KILLERS [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:f1a2d9ed04ee4b633e4110e154f1ead9be94c32e&dn=Game+of+Thrones+S03E05+PROPER+720p+HDTV+x264-KILLERS+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-29 06:54","size":"1.4 GiB","seeders":2333,"leechers":149,"trusted":true},{"id":8325644,"name":"Game of Thrones S03E01 720p HDTV x264-EVOLVE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:62b4a9a8656713d5cf58c5207e4f78e56e6295a1&dn=Game+of+Thrones+S03E01+720p+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-01 04:52","size":"1.3 GiB","seeders":2235,"leechers":160,"trusted":true},{"id":8468404,"name":"Game of Thrones S03E07 HDTV x264-2HD[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:1fb1d92b28dc7032081970f58c6b1bbc7a9da932&dn=Game+of+Thrones+S03E07+HDTV+x264-2HD%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-13 04:17","size":"443.29 MiB","seeders":2073,"leechers":77,"trusted":true},{"id":8421579,"name":"Game of Thrones S03E05 HDTV x264-2HD[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:c9efad8c2a34b2f2e5f88f04706f4676296cb2ad&dn=Game+of+Thrones+S03E05+HDTV+x264-2HD%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-29 04:06","size":"406.45 MiB","seeders":1913,"leechers":48,"trusted":true},{"id":8421694,"name":"Game of Thrones S03E05 PROPER HDTV x264-EVOLVE [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:2b39705e7bcb8fb03df3ed904a7a4dff06663bf7&dn=Game+of+Thrones+S03E05+PROPER+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-29 05:15","size":"394.73 MiB","seeders":1793,"leechers":47,"trusted":true},{"id":8374559,"name":"Game of Thrones S03E03 PROPER HDTV x264-2HD [eztv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:86559175dc3892783926ac0752ac6e31cbc689a9&dn=Game+of+Thrones+S03E03+PROPER+HDTV+x264-2HD+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-15 07:28","size":"400.7 MiB","seeders":1785,"leechers":83,"trusted":true},{"id":8492224,"name":"Game of Thrones S03E08 PROPER HDTV x264-2HD","category":"TV shows","magnet":"magnet:?xt=urn:btih:b3e09e42348e08180f34e5072824f460a5097629&dn=Game+of+Thrones+S03E08+PROPER+HDTV+x264-2HD&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-20 06:29","size":"369.94 MiB","seeders":1697,"leechers":38,"trusted":true},{"id":8498681,"name":"Game of Thrones Season 3 Episode 8","category":"TV shows","magnet":"magnet:?xt=urn:btih:76f408647c4694ed5a822a5772d5b73014f86e9d&dn=Game+of+Thrones+Season+3+Episode+8&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-22 09:00","size":"369.93 MiB","seeders":1546,"leechers":110,"trusted":false},{"id":8374343,"name":"Game of Thrones S03E03 PROPER HDTV x264-2HD[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:28ad28660dc173d389c9a28000b4213a10d9d6f9&dn=Game+of+Thrones+S03E03+PROPER+HDTV+x264-2HD%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-15 05:40","size":"400.7 MiB","seeders":1245,"leechers":56,"trusted":true},{"id":8421662,"name":"Game of Thrones S03E05 720p HDTV x264-IMMERSE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:8e7db1bc5f573173a20ff820ea7e36d6f952e646&dn=Game+of+Thrones+S03E05+720p+HDTV+x264-IMMERSE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-29 05:00","size":"1.11 GiB","seeders":1220,"leechers":93,"trusted":true},{"id":8442849,"name":"Game of Thrones S03E06 HDTV x264-2HD[ettv]","category":"TV shows","magnet":"magnet:?xt=urn:btih:c55f1e224f9056386fa72c147518a73d636a90aa&dn=Game+of+Thrones+S03E06+HDTV+x264-2HD%5Bettv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"05-06 04:01","size":"369.73 MiB","seeders":1164,"leechers":25,"trusted":true},{"id":8325878,"name":"Game of Thrones S03E01 REPACK 720p HDTV x264-EVOLVE [eztv]","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:bfdef4aafe3a5d9f007a96680eccf1d5084628f4&dn=Game+of+Thrones+S03E01+REPACK+720p+HDTV+x264-EVOLVE+%5Beztv%5D&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"04-01 06:46","size":"1.3 GiB","seeders":1126,"leechers":62,"trusted":true},{"id":8099420,"name":"Game.of.Thrones.S02.Season.2.COMPLETE.720p.BluRay.x264.MIKY","category":"HD - TV shows","magnet":"magnet:?xt=urn:btih:38360055ba1f51aaa749e55b25de563c910179d2&dn=Game.of.Thrones.S02.Season.2.COMPLETE.720p.BluRay.x264.MIKY&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337","uploaded":"02-03 21:51","size":"9.69 GiB","seeders":998,"leechers":828,"trusted":false}]"""

  }

  private def downloadShow(magnetLink: String) = {

    import scala.sys.process._

    val delugeDameonProcPath = """C:\Program Files (x86)\Deluge\deluged.exe"""
    val delugeConsoleProcPath = """C:\Program Files (x86)\Deluge\deluge-console.exe"""
    val delugeGuiProcPath = """C:\Program Files (x86)\Deluge\deluge.exe"""

    // The log messages are stored in here
    val out = new StringBuilder
    val err = new StringBuilder

    // The logger takes the messages of the processes
    val logger = ProcessLogger(
      (o: String) => out.append(o),
      (e: String) => err.append(e))

    // Run the daemon in the background so the torrent can be added
    val delugeDaemon = Process(delugeDameonProcPath) run logger

    val addCmd = "add"

    val magnetLinkWithQuotes = "\"" + magnetLink + "\""

    // Add the magnet link to deluge
    val delugeConsoleProc = Process(delugeConsoleProcPath + " " + addCmd + " " + magnetLink)
    val procOutput = delugeConsoleProc run logger

    // Run the process and save its output to the logger
    val delugeGuiProcess = Process(delugeGuiProcPath)
    val guiExitCode = delugeGuiProcess run logger

  }

}