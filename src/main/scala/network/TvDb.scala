package network

import data.PvrShow
import scala.xml.XML
import scala.io.Source
import java.net.URL
import data.PvrShow
import java.util.Date
import data.PvrShow
import scala.collection.immutable.HashMap
import data.PvrShow

object TvDb {
  
  // My key to the tvdb.com API
  val apiKey = "701786C7C08403C8"

  // Tags used in the XML received from thetvdb.com
  val showTag = "Series"; val showIdTag = "seriesid"; val showOverviewTag = "Overview"
  val showNameTag = "SeriesName"; val firstAiredTag = "FirstAired"; val imdbTag = "IMDB_ID"
  val networkTag = "Network"

  val showQueryStub = "http://thetvdb.com/api/GetSeries.php?seriesname="

  def getShowInfo(showName: String): Map[String, PvrShow] = {

    // There is no place for spaces in URL
    val formattedShowName = showName.trim().replaceAll(" ", "%20")

    val queryUrl = showQueryStub + formattedShowName

    // Get the site response as XML
    val xmlString = Source.fromURL(new URL(queryUrl)).mkString
    val xml = XML.loadString(xmlString)

    // The XML might contain multiple shows
    val shows = xml \\ showTag

    println("Found " + shows.size + " shows in TvDb:")
    var mapOfShow = new HashMap[String, PvrShow]
    for (show <- shows) {
      val showId = show \\ showIdTag text
      val returnedShowName = show \\ showNameTag text
      val overview = show \\ showOverviewTag text
      val imdbId = show \\ imdbTag text
      val network = show \\ networkTag text
      //  val firstAired = show \\ firstAiredTag text
      // val firstAirDate = new Date(firstAired)
      //firstAired = firstAirDate,

      val pvrShow = new PvrShow(name = returnedShowName, tvDbId = showId,
        description = overview, imdbId = imdbId)

      mapOfShow = mapOfShow + (showId -> pvrShow)
      println("Id: " + showId + "\nName: " + returnedShowName + "\nOverview: " + overview)
      println("################################")
    }

    //    response.foreach(line => println(line))

    mapOfShow
  }
  
  /*
   * <mirrorpath>/api/GetSeries.php?seriesname=<seriesname>


http://thetvdb.com/api/701786C7C08403C8/series/121361/all/en.zip
   */

}