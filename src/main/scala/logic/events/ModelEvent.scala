package logic.events

import data.PvrShow
import scala.swing.event.Event

abstract class ModelEvent() extends Event

case class ShowAdded(newShow: PvrShow) extends ModelEvent
