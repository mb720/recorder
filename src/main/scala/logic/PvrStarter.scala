/**
 *
 */
package logic

import gui.MainWindowControl
import data.PvrShow
import network.TorrentDownloader
import gui.UiUtils
import gui.strings.UiStringsEnglishUs
import data.Episode
import java.util.Date
import torrents.TorrentClient
import torrents.TorrentUtils
import torrents.TorrentClient
import torrents.TorrentUtils
import data.ModelForShows

/**
 * @author Matthias Braun
 *
 */
object PvrStarter extends App {

  println("PVR started")
  val episode = new Episode("Second Sons", new Date)
  val deluge = TorrentUtils.getClients()(TorrentUtils.DELUGE)
  val GoT = new PvrShow(name = "Game of Thrones", nextEpisode = episode,
    torrentSite = "www.thepiratebay.se", torrentClient = deluge)

  GoT addPublisher "eztv"
  GoT addPublisher "ettv"

  ModelForShows addShow GoT

  val episode2 = new Episode("Pilot", new Date)
  val arrestedDevelopment = new PvrShow(name = "Arrested Development", nextEpisode = episode2,
    torrentSite = "www.thepiratebay.se", torrentClient = deluge)

  arrestedDevelopment addPublisher "eztv"
  arrestedDevelopment addPublisher "ettv"

  ModelForShows addShow arrestedDevelopment

  MainWindowControl.start

}
