/**
 *
 */
package data

import scala.collection.immutable.HashSet
import java.util.Date
import torrents.TorrentClient
import torrents.TorrentClient
import torrents.TorrentClient

/**
 * Represents a torrent subscription including the generic name of the series and the list of preferred publishers.
 *
 * @author Matthias Braun
 *
 */

class PvrShow(var name: String = "", var active: Boolean = true, var torrentSite: String = "",
  var torrentClient: TorrentClient = TorrentClient(),
  var description: String = "no description", var tvDbId: String = "", var imdbId: String = "",
  var firstAired: Date = new Date, var network: String = "", var bannerPath: String = "",
  var nextEpisode: Episode = new Episode("", new Date)) {

  /**
   * The set of the trusted publishers' names for this show
   */
  private var _trustedPublishers = new HashSet[String]

  def addPublisher(publisher: String) = _trustedPublishers += publisher
  def removePublisher(publisher: String) = _trustedPublishers -= publisher

  def trustedUploaders = _trustedPublishers

  def toArray(): Array[AnyRef] = {

    var publishersStr = ""
    if (!_trustedPublishers.isEmpty)
      publishersStr = _trustedPublishers.reduce { (acc, pubName) =>
        acc + ", " + pubName
      }

    Array(name, nextEpisode.name, publishersStr, torrentSite, nextEpisode.airDate,
      torrentClient.name, active.asInstanceOf[java.lang.Boolean])

  }

}
