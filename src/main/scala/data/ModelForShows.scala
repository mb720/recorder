package data

import scala.swing.Publisher
import logic.events.ShowAdded

object ModelForShows extends Publisher {

  private var _listOfShows: List[PvrShow] = List()

  def listOfShows = _listOfShows

  def addShow(newShow: PvrShow): Unit = {
    _listOfShows = _listOfShows ::: List(newShow)

    val event = new ShowAdded(newShow)
    publish(event)

    //    println("All " + _listOfShows.size + " shows:")
    //    _listOfShows.foreach(show => println(show.presentationName))
  }

  def removeShow(showToRemove: PvrShow): Unit = { _listOfShows - showToRemove }

}
