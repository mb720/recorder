package torrents

import scala.collection.immutable.HashMap

object TorrentUtils {

  val DELUGE = "Deluge"
  val UTORRENT = "µTorrent"
  val TRANSMISSION = "Transmission"
  val QBITTORRENT = "qBittorrent"

  val DELUGE_ICON = "/gui/images/deluge.png"
  val UTORRENT_ICON = "/gui/images/utorrent.png"
  val TRANSMISSION_ICON = "/gui/images/transmission.png"
  val QBITTORRENT_ICON = "/gui/images/qbittorrent.png"

  def getClients(): HashMap[String, TorrentClient] = {

    val installDir = ""
    val deluge = new TorrentClient(DELUGE, installDir, DELUGE_ICON, addMagnetInDeluge _)
    val uTorrent = new TorrentClient(UTORRENT, installDir, UTORRENT_ICON, addMagnetInDeluge _)
    val transmission = new TorrentClient(TRANSMISSION, installDir, TRANSMISSION_ICON, addMagnetInDeluge _)
    val qbittorrent = new TorrentClient(QBITTORRENT, installDir, QBITTORRENT_ICON, addMagnetInDeluge _)

    HashMap(DELUGE -> deluge, UTORRENT -> uTorrent,
      TRANSMISSION -> transmission, QBITTORRENT -> qbittorrent)

  }

  def addMagnetInDeluge(magnetLink: String, delugeDir: String): Unit = {

    import scala.sys.process._

    val delugeDameonProcPath = """C:\Program Files (x86)\Deluge\deluged.exe"""
    val delugeConsoleProcPath = """C:\Program Files (x86)\Deluge\deluge-console.exe"""
    val delugeGuiProcPath = """C:\Program Files (x86)\Deluge\deluge.exe"""

    // The log messages are stored in here
    val out = new StringBuilder
    val err = new StringBuilder

    // The logger takes the messages of the processes
    val logger = ProcessLogger(
      (o: String) => out.append(o),
      (e: String) => err.append(e))

    // Run the daemon in the background so the torrent can be added
    val delugeDaemon = Process(delugeDameonProcPath) run logger

    val addCmd = "add"

    val magnetLinkWithQuotes = "\"" + magnetLink + "\""

    // Add the magnet link to deluge
    val delugeConsoleProc = Process(delugeConsoleProcPath + " " + addCmd + " " + magnetLink)
    val procOutput = delugeConsoleProc run logger

    // Run the process and save its output to the logger
    val delugeGuiProcess = Process(delugeGuiProcPath)
    val guiExitCode = delugeGuiProcess run logger
  }
}
