/**
 *
 */
package torrents

/**
 * Represents a torrent client that downloads content using torrent files.
 *
 *
 *
 * @author Matthias Braun
 *
 */
class TorrentClient(var name: String, var installDir: String, var pathToPic: String,
  var addTorrent: (String, String) => _) {

}

object TorrentClient {

  def doNothing(x: String, y: String) = {}

  def apply() = {
    new TorrentClient(name = "", installDir = "", pathToPic = "", doNothing _)
  }
}