package gui.strings
/**
 * The objects of this trait provide the strings for the GUI in one language
 */
trait GuiStrings {

  def addShowButtonText: String
  def addShowButtonTooltip: String

  def findShowButtonText: String

  def mainWindowTitle: String
  def addShowDialogTitle: String
  def nameOfShow: String
  def nextEpisodeName: String
  def trustedUploaders: String
  def torrentSite: String
  def nextEpisodeDate: String
  def active: String
  def torrentClient: String
  def chooseTorrentClient: String
  def chooseTorrentClientDialogTitle(showName: String): String

  /**
   *   The headers of the show table's columns. The user can change this Array by adding or removing titles (or changing their order)
   */
  def showTableHeaders = Array[AnyRef](nameOfShow, nextEpisodeName, trustedUploaders, torrentSite, nextEpisodeDate, torrentClient, active)

}
