package gui.strings

/**
 * The strings for the english (US) user interface
 * @author Matthias Braun
 */
object UiStringsEnglishUs extends GuiStrings {

  private val ADD_SHOW_BUTTON_TEXT = "Add Show"
  private val ADD_SHOW_BUTTON_TOOLTIP = "This show's episodes will be downloaded automatically"

  private val MAIN_WINDOW_TITLE = "Scala PVR"

  private val ADD_SHOW_DIALOG_TITLE = "Add a new show"
  private val FIND_SHOW_BUTTON_TEXT = "Find show"
  private val NAME_OF_SHOW = "Name of show"
  private val NEXT_EPISODE_NAME = "Next episode name"
  private val TRUSTED_UPLOADERS = "Trusted uploaders"
  private val TORRENT_SITE = "Torrent site"
  private val NEXT_EPISODE_DATE = "Next episode date"
  private val ACTIVE = "active"
  private val TORRENT_CLIENT = "Torrent client"
  private val CHOOSE_TORRENT_CLIENT = "Choose your torrent client"
  private val CHOOSE_TORRENT_CLIENT_DIALOG_TITLE = "Choose the torrent client for "

  def chooseTorrentClient = CHOOSE_TORRENT_CLIENT
  def addShowButtonText = ADD_SHOW_BUTTON_TEXT
  def addShowButtonTooltip = ADD_SHOW_BUTTON_TOOLTIP

  def findShowButtonText = FIND_SHOW_BUTTON_TEXT
  def mainWindowTitle = MAIN_WINDOW_TITLE
  def addShowDialogTitle = ADD_SHOW_DIALOG_TITLE
  def nameOfShow = NAME_OF_SHOW
  def nextEpisodeName = NEXT_EPISODE_NAME
  def trustedUploaders = TRUSTED_UPLOADERS
  def torrentSite = TORRENT_SITE
  def nextEpisodeDate = NEXT_EPISODE_DATE
  def active = ACTIVE
  def torrentClient = TORRENT_CLIENT
  def chooseTorrentClientDialogTitle(showName: String) = CHOOSE_TORRENT_CLIENT_DIALOG_TITLE + showName

}
