package gui

import gui.strings.UiStringsEnglishUs
import gui.strings.GuiStrings
import java.awt.Color
import java.io.File

object UiUtils {

  object Icons {
    private def appIcon = "/gui/images/download.png"
    def app = getUrl(appIcon)

    private def findShowIcon = "/gui/images/find.png"
    def findShow = getUrl(findShowIcon)

    private def addShowIcon = "/gui/images/add.png"
    def addShow = getUrl(addShowIcon)

    def getUrl(path: String) = {
      getClass.getResource(path)
    }
  }

  object Colors {

    def green = new Color(0x2EB82E)
    def veryLightBlue = new Color(0xCCDFEE)
  }
  // The language per default is english (US)
  private var _strings: GuiStrings = UiStringsEnglishUs

  def setLanguage(stringsOfOneLanguage: GuiStrings) {
    _strings = stringsOfOneLanguage;

  }
  def strings: GuiStrings = { _strings }

  /**
   * Return a start directory for the user to look for a torrent client.
   */
  def getStartDirForTorrentClient: Option[File] = {

    val windowsStartDir = new File("""C:\""")
    val unixStartDir = new File("/")
    val operatingSystem: String = System.getProperty("os.name").toLowerCase()
    if (operatingSystem.contains("windows") && windowsStartDir.listFiles != null) {
      return Some(windowsStartDir)
    } else if (unixStartDir.listFiles() != null) {
      return Some(unixStartDir)
    } else { return None }

  }

}
