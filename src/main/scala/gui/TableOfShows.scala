package gui
import swing.event._
import scala.swing._
import data.PvrShow
import java.awt.event.MouseAdapter

class TableOfShows extends Component {
  val mainColor = UiUtils.Colors.veryLightBlue
  val ui = new BoxPanel(Orientation.Vertical) {
    background = mainColor
    val table = new Table() {

      model = MainWindowControl.tableModel
      background = mainColor
    }
    // The user can select one row at a time
    table.selection.intervalMode = Table.IntervalMode.Single
    table.selection.elementMode = Table.ElementMode.Row
    val scrollPane = new ScrollPane(table)
    scrollPane.background = mainColor
    contents += scrollPane
    visible = true

    //    val header = table.peer.getTableHeader()
    //
    //    header.addMouseListener(new MouseAdapter() {
    //      def mouseClicked(e: MouseEvent) {
    //        val col = header.columnAtPoint(e.point)
    ////        val row = table.model.
    //
    //        println("Column header " + col + " selected")
    //      }
    //    })
  }

}

