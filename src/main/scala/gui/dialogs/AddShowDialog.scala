package gui.dialogs

import scala.swing._
import swing._
import javax.swing.ImageIcon
import data.PvrShow
import gui.UiUtils
import data.Episode
import java.util.Date
import torrents.TorrentUtils
import java.awt.Dimension
import network.TvDb
import javax.swing.border.LineBorder
import scala.swing.event._
import javax.swing.JTextPane
import javax.swing.border.EmptyBorder
import concurrent.ops._
import scala.collection.immutable.HashMap
import data.ModelForShows

object AddShowDialog extends Dialog {

  title = UiUtils.strings.addShowDialogTitle
  preferredSize = new Dimension(350, 400)
  // If the dialog is modal, the main window doesn't respond while the dialog is active
  modal = false

  def start() {

    val findShowPanel = new BoxPanel(Orientation.Vertical)

    val showNamelabel = new Label(UiUtils.strings.nameOfShow)
    val showNameTextField = new TextField
    showNameTextField.maximumSize = new Dimension(preferredSize.width, 30)

    val findShowButton = new Button(UiUtils.strings.findShowButtonText)
    findShowButton.icon = new ImageIcon(UiUtils.Icons.findShow)

    findShowPanel.contents += showNamelabel
    findShowPanel.contents += showNameTextField
    findShowPanel.contents += findShowButton

    // React when the user clicks the 'find show' button or hits enter in the show's textfield
    listenTo(findShowButton, showNameTextField.keys)

    reactions += {
      case ButtonClicked(`findShowButton`) =>
        println(findShowButton.text + " was clicked in the dialog")
        spawn {
          displayShows(findShowPanel, showNameTextField, findShowButton)

        }
      case KeyPressed(`showNameTextField`, Key.Enter, _, _) =>
        println("showNameTextField enter key pressed")
        // Hitting enter in the text field has the same effect as clicking the button
        findShowButton.doClick

    }

    contents = findShowPanel

    centerOnScreen()
    open()
  }

  private def displayShows(findShowPanel: BoxPanel, showNameTextField: TextField, findShowButton: Button): Unit = {

    val allShowsPanel = new BoxPanel(Orientation.Vertical)

    val showName = showNameTextField.text

    val showMap = TvDb.getShowInfo(showName)

    val addShowButton = new Button(UiUtils.strings.addShowButtonText)
    addShowButton.icon = new ImageIcon(UiUtils.Icons.addShow)
    val addShowPanel = new BoxPanel(Orientation.Vertical)
    addShowPanel.contents += addShowButton

    allShowsPanel.contents += new BorderPanel {
      add(findShowPanel, BorderPanel.Position.West)
      add(addShowPanel, BorderPanel.Position.East)

    }

    // The border a show's info panel has when it is selected
    val selectedBorder = new LineBorder(UiUtils.Colors.green, 4)

    // The border a show's info panel has when it is not selected
    val defaultBorder = new LineBorder(UiUtils.Colors.veryLightBlue, 2)

    // A mapping from show id to the show's info panels
    var showPanelMap = new HashMap[String, Panel]

    for (show <- showMap.values) {

      // Info for one single show
      val showInfoPanel = new BoxPanel(Orientation.Vertical)
      showInfoPanel.name = show.tvDbId
      showInfoPanel.border = defaultBorder
      //  showInfoPanel.contents += new Label("First air date:")
      //          showInfoPanel.contents += new Label(show.firstAired.toGMTString())
      showInfoPanel.contents += new Label(show.name)
      val descriptionArea = new TextArea(show.description)
      descriptionArea.lineWrap = true
      descriptionArea.editable = false
      descriptionArea.focusable = false;
      descriptionArea.name = show.tvDbId

      // The textarea needs its own mouse click listener
      listenTo(descriptionArea.mouse.clicks)

      showPanelMap = showPanelMap + (show.tvDbId -> showInfoPanel)
      showInfoPanel.contents += descriptionArea

      listenTo(showInfoPanel.mouse.clicks, addShowButton)
      allShowsPanel.contents += showInfoPanel

    }

    var selectedShowId = ""
    // When there are more or less than one show, the user has to search again or choose one show
    if (showMap.size == 1) {
      addShowButton.enabled = true
      selectedShowId = showMap.keySet.head
      showPanelMap.values.head.border = selectedBorder
    } else { addShowButton.enabled = false }

    // val scrollPane = new ScrollPane(allShowsPanel)
    contents = allShowsPanel

    reactions += {

      case e: MousePressed =>
        // Reset all the show's borders to default
        allShowsPanel.contents.foreach(comp => comp.border = defaultBorder)
        // It's expected that the panel's name is the key to the map of shows
        val showId = e.source.name
        if (showPanelMap.contains(showId)) {
          showPanelMap(showId).border = selectedBorder
        }
        selectedShowId = showId

        addShowButton.enabled = true

      case ButtonClicked(`addShowButton`) =>
        if (selectedShowId != "") {
          ModelForShows.addShow(showMap(selectedShowId))
          this.close

        }

    }

  }
}
