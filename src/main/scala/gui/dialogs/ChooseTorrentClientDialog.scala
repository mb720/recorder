package gui.dialogs

import scala.swing._
import javax.swing.ImageIcon
import gui.UiUtils
import scala.swing.event.ButtonClicked
import java.awt.Color
import java.io.File
import scala.collection.immutable.HashSet
import scala.swing.Label
import torrents.TorrentClient
import scala.collection.immutable.HashMap
import torrents.TorrentUtils
import data.PvrShow
import javax.swing.border.EmptyBorder
import java.awt.Font
import gui.TableOfShows
import gui.MainWindowControl

class ImageButton(img: String) extends Button {
  icon = new ImageIcon(getClass.getResource(img))
  borderPainted = false
  focusable = false
  background = UiUtils.Colors.veryLightBlue
}

class ChooseTorrentClientDialog(show: PvrShow, clients: HashMap[String, TorrentClient]) extends Dialog {

  title = UiUtils.strings.chooseTorrentClientDialogTitle(show.name)
 
  // If the dialog is modal, the main window doesn't respond while the dialog is active
  modal = false
  def mainColor = UiUtils.Colors.veryLightBlue

  val chooseClientLabel = new Label("Choose your torrent client")
  chooseClientLabel.font = new Font(null, Font.BOLD, 30);
  chooseClientLabel.horizontalAlignment = Alignment.Right
  //  chooseClientLabel.size = new Dimension(size.width, 40)
  // Create image buttons for each supported torrent client and attach listener to them
  val delugeButt = new ImageButton(TorrentUtils.DELUGE_ICON)
  val uTorrentButt = new ImageButton(TorrentUtils.UTORRENT_ICON)
  val qBittorrentButt = new ImageButton(TorrentUtils.QBITTORRENT_ICON)
  val transmissionButt = new ImageButton(TorrentUtils.TRANSMISSION_ICON)
  listenTo(delugeButt, uTorrentButt, qBittorrentButt, transmissionButt)

  reactions += {

    case ButtonClicked(`delugeButt`) =>
      println("deluge was clicked in the dialog")
      val dialog = new AskForDelugeDir()
      new ConfirmOrChangeTorrentClientDir(dialog.lookInCommonDirs, size, show, TorrentUtils.DELUGE)
    case ButtonClicked(`uTorrentButt`) =>
      println("µTorrent was clicked in the dialog")
    case ButtonClicked(`qBittorrentButt`) =>
      println("qBittorrent was clicked in the dialog")
    case ButtonClicked(`transmissionButt`) =>
      println("transmission was clicked in the dialog")
  }

  def showTorrentsSelection() = {

    val selectionPanel = new BoxPanel(Orientation.Vertical)
    selectionPanel.background = mainColor
    //    selectionPanel.contents += chooseClientLabel
    val torrentsPanel = new GridPanel(2, 2)

    torrentsPanel.contents += delugeButt
    torrentsPanel.contents += uTorrentButt
    torrentsPanel.contents += qBittorrentButt
    torrentsPanel.contents += transmissionButt
    selectionPanel.contents += torrentsPanel

    contents = selectionPanel

    centerOnScreen()
    open()
  }

  /**
   * Ask the user to confirm or change the torrent client's installation directory.
   */
  class ConfirmOrChangeTorrentClientDir(dir: Option[String], size: Dimension, show: PvrShow, clientName: String) {
    val mainPanel = new BoxPanel(Orientation.Vertical)
    mainPanel.preferredSize = size
    mainPanel.background = mainColor

    // Those buttons are used to confirm the torrent client's installation directory
    val buttonPanel = new BoxPanel(Orientation.Horizontal)
    buttonPanel.border = new EmptyBorder(20, 10, 10, 10)
    buttonPanel.background = mainColor
    val okButton = new Button("OK")
    val backButton = new Button("Back")
    val browseButton = new ImageButton("/gui/images/browse_folder.png")
    buttonPanel.contents += backButton
    buttonPanel.contents += okButton

    listenTo(okButton, backButton, browseButton)

    val dirTextField = new TextField()
    val directoryPanel = new BoxPanel(Orientation.Horizontal)
    directoryPanel.background = mainColor
    directoryPanel.contents += dirTextField
    directoryPanel.contents += browseButton

    dirTextField.maximumSize = new Dimension(size.width, 20)
    dir match {
      case Some(dir) =>
        mainPanel.contents += new Label("Found " + clientName + " in this directory:")

        dirTextField.text = dir
        mainPanel.contents += directoryPanel
        mainPanel.contents += new Label("You can change this by clicking the directory icon.")
      case None =>
        mainPanel.contents += new Label("Please select " + clientName + "'s installation directory")
        mainPanel.contents += directoryPanel
    }
    mainPanel.contents += buttonPanel

    contents = mainPanel

    reactions += {
      case ButtonClicked(`okButton`) =>
        println("OkButton clicked")
        show.torrentClient.installDir = dirTextField.text
        show.torrentClient.name = clientName
        println("Chosen torrent client: " + show.torrentClient.name)
        // Update the table to reflect the changes
        MainWindowControl.updateTable
        close()
      case ButtonClicked(`backButton`) =>
        println("BackButton clicked")
        showTorrentsSelection()

      case ButtonClicked(`browseButton`) =>
        println("BrowseButton clicked")
        var fileChooser = new FileChooser()
        UiUtils.getStartDirForTorrentClient match {
          case Some(dir: File) =>
            fileChooser = new FileChooser(dir)
          case None =>
            fileChooser = new FileChooser()

        }

        fileChooser.title = "Select " + clientName + "'s installation directory"
        fileChooser.fileSelectionMode = FileChooser.SelectionMode.DirectoriesOnly

        if (fileChooser.showOpenDialog(mainPanel) == FileChooser.Result.Approve) {
          val selectedDir = fileChooser.selectedFile.getAbsolutePath
          dirTextField.text = selectedDir
        }
    }
  }
}

/**
 * The app tries to find the location of the torrent clients' executables automatically.
 * The user validates the location if it was found or specifies the location manually.
 */
abstract class TorrentClientLocationDialog extends Dialog {

  protected def lookInCommonDirs: Option[String]
  protected def askUserForLocation

  /**
   * A template method to get the torrent client's location
   */
  def getLocation = {
    if (lookInCommonDirs == None) {
      askUserForLocation
    } else {
      contents
    }
  }

}

/*
 * It makes only sense to define case classes if pattern matching is used to decompose data structures.
 */

class AskForDelugeDir() extends TorrentClientLocationDialog {

  // Places where deluge is often installed on Windows
  val windowsCommonDirs: Set[String] = HashSet("""C:\Program Files (x86)\Deluge""", """C:\Program Files\Deluge""")
  val delugeFiles = HashSet("deluge.exe", "deluged.exe", "deluge-console.exe")

  def lookInCommonDirs: Option[String] = {
    for (dir <- windowsCommonDirs) {
      val files = new File(dir).listFiles
      if (files != null) {
        // All of Deluge's necessary files are in this directory ->
        if (delugeFiles.forall(delugeFile => files.exists(file => file.getName.equals(delugeFile)))) {
          return Some(dir)
        }
      }

    }
    // None of the common directories is the home of Deluge
    None
  }

  protected def askUserForLocation = {
    println("Ask user for Deluge's installation directory")

    //     val fileChooser = new FileChooser()
    //     fileChooser.title = "Select Deluge's installation directory"
    //     fileChooser.showOpenDialog(this)
    /*
       *          if(configFileChooser.showOpenDialog(channelSelectPanel) ==
       * FileChooser.Result.Approve) {
       * configFileName = configFileChooser.selectedFile.getAbsolutePath
       */
  }
}
