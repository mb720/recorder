package gui

import javax.swing.table.DefaultTableModel
import torrents.TorrentUtils
import scala.swing._
import gui.dialogs._
import network.TorrentDownloader
import scala.swing.event._
import logic.events.ShowAdded
import data.PvrShow
import concurrent.ops._
import data.ModelForShows

object MainWindowControl extends Component {

  var tableModel = new DefaultTableModel
  val mainWindow = MainWindow

  def updateTable() = {
    tableModel = new DefaultTableModel
    tableModel.setColumnIdentifiers(UiUtils.strings.showTableHeaders)
  
    // Fill the table model with shows
    ModelForShows.listOfShows.foreach(show => tableModel.addRow(show.toArray))
    mainWindow.tableOfShows.ui.table.model = tableModel
  }

  def start() = {
    println("MainWindowControl started")


    tableModel.setColumnIdentifiers(UiUtils.strings.showTableHeaders)
    // Fill the table model with shows
    ModelForShows.listOfShows.foreach(show => tableModel.addRow(show.toArray))

    listenTo(mainWindow.tableOfShows.ui.table.selection)
    listenTo(mainWindow.addShowButton, mainWindow.testButton)
    listenTo(ModelForShows)

    val torrentClients = TorrentUtils.getClients()

    reactions += {

      case ButtonClicked(mainWindow.addShowButton) =>
        println(mainWindow.addShowButton.text + " was pressed")

        val addShowDialog = AddShowDialog.start()

      case ButtonClicked(mainWindow.testButton) =>
        println(mainWindow.testButton.text + " was pressed")

        val deluge = torrentClients(TorrentUtils.DELUGE)
        // Download the next episode of the first show with deluge (in its own thread)
        spawn {
          TorrentDownloader.download(ModelForShows.listOfShows.head, deluge)
        }
      case TableRowsSelected(source, range, false) =>
        println("TableRowsSelected")
        val col = source.selection.columns.leadIndex
        val row = source.selection.rows.leadIndex
        println("col: " + col + ", row: " + row)
        if (row != -1 && source.model.getColumnName(col).equals(UiUtils.strings.torrentClient)) {
          println("Torrent client pressed")
          println("ModelForShows.listOfShows.size: " + ModelForShows.listOfShows.size)
          val show = ModelForShows.listOfShows(row)
          val chooseTorrent = new ChooseTorrentClientDialog(show, torrentClients)

          chooseTorrent.showTorrentsSelection

          println(show.name + "s client: " + show.torrentClient.name)

        }
      case TableColumnsSelected(source, range, false) =>
        println("TableColumnsSelected")

      case ShowAdded(show: PvrShow) =>
        tableModel.addRow(show.toArray)

    }

  }
}
